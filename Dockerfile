# Specify exact version to ensure updates are controlled
FROM golang:1.12.5 as builder

# Let people know who is maintaining this image
LABEL maintainer="Eric Liu <eric@nomoss.co>"

# Install in standard location for Go applications
WORKDIR $GOPATH/src/gitlab.com/eric-liu/devops-template

# Copy all existing files from current directory (on host) to working directory in docker container
COPY . .

# Compile the Go application executable (statically linked to be smaller image size)
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /go/bin/devops-template .


####### Start a separate stage from Alpine linux (smaller image) #######
FROM alpine:3.9.4

WORKDIR /root/

# Copy pre-built binary from previous stage
COPY --from=builder /go/bin/devops-template .

# Allow port 8000 to be exposed by the container
EXPOSE 8000

# Run the executable by default (when no other option is specified)
CMD ["./devops-template"]
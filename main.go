// Server2 is a minimal "echo" and counter server.
package main

import (
	"fmt"
	"image"
	"image/color"
	"image/gif"
	"io"
	"log"
	"math"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"sync"
)

var mu sync.Mutex
var count int

func main() {
	log.Println("Starting server")
	http.HandleFunc("/", handler)
	http.HandleFunc("/count", counter)
	http.HandleFunc("/lissajous", func(w http.ResponseWriter, r *http.Request) {
		lissajous(w, r)
	})

	log.Println("Server running")
	if err := http.ListenAndServe("0.0.0.0:8000", nil); err != nil {
		log.Fatal(err)
	}
	log.Println("Shutting down")
}

// handler echoes the HTTP request.
func handler(w http.ResponseWriter, r *http.Request) {
	updateCount()

	fmt.Fprintf(w, "%s %s %s\n", r.Method, r.URL, r.Proto)
	for k, v := range r.Header {
		fmt.Fprintf(w, "Header[%q] = %q\n", k, v)
	}
	fmt.Fprintf(w, "Host = %q\n", r.Host)
	fmt.Fprintf(w, "RemoteAddr = %q\n", r.RemoteAddr)
	if err := r.ParseForm(); err != nil {
		log.Print(err)
	}
	for k, v := range r.Form {
		fmt.Fprintf(w, "Form[%q] = %q\n", k, v)
	}
}

// counter echoes the number of calls so far.
func counter(w http.ResponseWriter, r *http.Request) {
	mu.Lock()
	fmt.Fprintf(w, "Count %d\n", count)
	mu.Unlock()
}

func lissajous(out io.Writer, r *http.Request) {
	updateCount()

	var palette = []color.Color{color.Black, color.White}
	var params, _ = url.ParseQuery(r.URL.RawQuery)

	cycles := 5 // number of complete x oscillator revolutions
	if params["cycles"] != nil {
		cycles, _ = strconv.Atoi(params["cycles"][0])
	}
	res := 0.0001 // angular resolution
	if params["res"] != nil {
		res, _ = strconv.ParseFloat(params["res"][0], 32)
	}
	size := 300 // image canvas covers [-size..+size]
	if params["size"] != nil {
		size, _ = strconv.Atoi(params["size"][0])
	}
	nframes := 128 // number of animation frames
	if params["nframes"] != nil {
		nframes, _ = strconv.Atoi(params["nframes"][0])
	}
	delay := 3 // delay between frames in 10ms units
	if params["delay"] != nil {
		delay, _ = strconv.Atoi(params["delay"][0])
	}

	freq := rand.Float64() * 3.0 // relative frequency of y oscillator
	anim := gif.GIF{LoopCount: nframes}
	phase := 0.0 // phase differences
	for i := 0; i < nframes; i++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < float64(cycles)*2*math.Pi; t += res {
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			outx := size + int(x*float64(size)+0.5)
			outy := size + int(y*float64(size)+0.5)
			img.SetColorIndex(outx, outy, 1)
		}
		phase += 0.05
		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}
	gif.EncodeAll(out, &anim) // NOTE: ignoring encoding errors
}

func updateCount() {
	mu.Lock()
	count++
	mu.Unlock()
}
